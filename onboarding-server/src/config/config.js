'use strict'

const { Pool } = require('pg')

const pool = new Pool({
   user: process.env['DB_USERNAME'],
   host: process.env['DB_HOSTNAME'],
   database: process.env['DB_NAME'],
   password: process.env['DB_PASSWORD'],
   port: process.env['DB_PORT'],
   ssl: true
})

// To do: Central Logging
console.log('Pool Connection is established.')

module.exports.pool = pool;