const pool = require('../config/config').pool;
const { response, request } = require('express');
const utility = require(`../../utility`);
const format = require('pg-format');
var md5 = require('md5');

const getPartnerDetails = (request, response, next) => {
  try {
    queryString = 'SELECT d.field_id, f.field_name, d.field_value, f.field_type, f.is_active, d.updated_on FROM partnerhandlerschema.partner_details d INNER JOIN partnerhandlerschema.partner_fields f ON f.id = d.field_id WHERE d.partner_id = $1;'
    pool.query(queryString, [request.params.id], (error, results) => {
      if (error) {
        next(error)
      } else {
        response.status(200).json({ 'status': 'success', 'message': '', 'count': results.rows.length, 'data': results.rows })
      }
    })
  }
  catch (err) {
    next(err);
  }
}
const insertFields = (insertArray) => {
  if (insertArray.length !== 0) {
    return new Promise((resolve, reject) => {
      var values = insertArray.map(a => {
        return `('${a.field_name}', '${a.field_type}', '${a.is_active}')`;
      });
      var detailsQuery = `INSERT INTO partnerhandlerschema.partner_fields(
    field_name, field_type, is_active) VALUES `;
      str = detailsQuery + values.join(',');
      pool.query(str, (error, results) => {
        if (error) {
          reject(err);
          return;
        }
        resolve(results.rowCount);
      });
    });
  }
};
const updateFields = (updateArray) => {
  if (updateArray.length !== 0) {
    return new Promise((resolve, reject) => {
      var values = updateArray.map(a => {
        return `(${a.id}::integer, '${a.field_name}'::text,'${a.field_type}'::text,${a.is_active}::boolean )`;
      });
      var query = `UPDATE partnerhandlerschema.partner_fields f
          SET field_name = v.field_name,
          field_type = v.field_type,
          is_active = v.is_active
          FROM (VALUES `+ values.join(',') + `)
          AS v (id,field_name,field_type,is_active)
          WHERE f.id = v.id` ;

      pool.query(query, (error, results) => {
        if (error) {
          reject(err);
          return;
        }
        resolve(results.rowCount);
      });
    });
  }
}
const deleteFields = (deleteArray) => {
  if (deleteArray.length !== 0) {
    return new Promise((resolve, reject) => {
      var values = deleteArray.map(a => {
        return a.id;
      });
      var queryDetail = `DELETE FROM partnerhandlerschema.partner_details 
        WHERE field_id IN (`+ values.join(',') + `)`;
      pool.query(queryDetail, (error, results) => {
        if (error) {
          throw error;
        } else {
          var queryFields = `DELETE FROM  partnerhandlerschema.partner_fields 
            WHERE id IN (`+ values.join(',') + `)`;
          pool.query(queryFields, (error, results) => {
            if (error) {
              reject(err);
              return;
            }
            resolve(results.rowCount);
          });
        }
      });
    });
  }
}
const submitPartnerFields = (request, response) => {
  const { partnerDetails } = request.body;
  var insertArray = [], deleteArray = [], updateArray = [];
  partnerDetails.forEach(partnerDetails => {
    var partners = {
      id: partnerDetails.field_id,
      field_name: partnerDetails.field_name,
      field_type: partnerDetails.field_type,
      is_active: partnerDetails.is_active,
    };
    switch (partnerDetails.flag) {
      case 'I':
        insertArray.push(partners);
        break;
      case 'U':
        updateArray.push(partners);
        break;
      case 'D':
        deleteArray.push(partners);
        break;
    }
  });
  const iPromise = insertFields(insertArray);
  const uPromise = updateFields(updateArray);
  const dPromise = deleteFields(deleteArray);

  Promise.all([iPromise, uPromise, dPromise])
    .then(responses => {
      console.log('submitPartnerFields: results', responses)
      response.json({
        status: 'Success',
        data: [{
          insertedRows: responses[0] || 0,
          updatedRows: responses[1] || 0,
          deletedRows: responses[2] || 0,
        }]
      })
    })
    .catch(err => {
      console.error(err);
      response.json({
        status: 'Error',
        error: err
      });
    });
}

const getPartnerFields = (request, response, next) => {
  try {
    queryString = 'SELECT id AS field_id, field_name, field_type, is_active FROM partnerhandlerschema.partner_fields'
    pool.query(queryString, (error, results) => {
      if (error) {
        next(error)
      } else {
        response.status(200).json({ 'status': 'success', 'message': '', 'count': results.rows.length, 'data': results.rows })
      }
    })
  }
  catch (err) {
    next(err);
  }
}

const getPartnerEndpointInfo = (request, response, next) => {
  try {
    pool.query('Select id as partnerEndpointId, display_name, url from partnerhandlerschema.partner_endpoints where endpoint_id = $1 and partner_id = $2',
      [request.params.sourceEndpointId, request.params.partnerId], (error, results) => {
        console.log(error, results);
        if (error) {
          next(error);
        } else {
          response.json({ message: 'success', count: results.rows.length, data: results.rows })
        }
      })
  } catch (err) {
    next(err);
  }

}

const getPartnerMappings = (request, response, next) => {
  try {
    let queryString;
    if (request.params.type === 'request') {
      queryString = `SELECT A.id as mappingId, key_name, mapping_value 
                    FROM partnerhandlerschema.request_mappings as A 
                    INNER JOIN partnerhandlerschema.source_endpoint_request_keys as B
                    ON A.key_id = B.id WHERE partner_endpoint_id = $1;`
    } else if (request.params.type === 'response') {
      queryString = `SELECT A.id as mappingId, key_name, mapping_value 
      FROM partnerhandlerschema.response_mappings as A 
      INNER JOIN partnerhandlerschema.source_endpoint_response_keys as B
      ON A.key_id = B.id WHERE partner_endpoint_id = $1;`
    }
    pool.query(queryString, [request.params.partnerEndpointId], (error, results) => {
      if (error) {
        next(error);
      } else {
        response.json({ message: 'success', count: results.rows.length, data: results.rows })
      }
    })
  } catch (err) {
    next(err);
  }
}

const getKeysbyEndpoint = (request, response, next) => {
  try {
    let queryString;
    if (request.params.type === 'request') {
      queryString = 'SELECT id,key_name FROM partnerhandlerschema.source_endpoint_request_keys WHERE source_endpoint_id = $1'
    } else if (request.params.type === 'response') {
      queryString = 'SELECT id,key_name FROM partnerhandlerschema.source_endpoint_response_keys WHERE source_endpoint_id = $1'
    }
    pool.query(queryString, [request.params.id], (error, results) => {
      if (error) {
        next(error)
      } else {
        response.status(200).json({ 'status': 'success', 'message': '', 'count': results.rows.length, 'data': results.rows })
      }
    })
  }
  catch (err) {
    next(err)
  }
}

const getAvailableEndpoints = (request, response, next) => {
  try {
    pool.query('SELECT id,name FROM partnerhandlerschema.source_endpoints', (error, results) => {
      if (error) {
        next(error)
      } else {
        response.status(200).json({ 'status': 'success', 'message': '', 'count': results.rows.length, 'data': results.rows })
      }
    })
  }
  catch (err) {
    next(err)
  }

}

const createPartner = (request, response) => {
  const { userId, isActive, partnerDetails } = request.body;
  // INSERT TO PARTNER 
  var insertPartnerSQL = `INSERT INTO partnerhandlerschema.partners(
    user_id, is_active, created_on, updated_on)
    VALUES ( $1, $2, $3, $3)
    RETURNING id`;
  pool.query(insertPartnerSQL,
    [userId, isActive, new Date(Date.now()).toISOString()],
    (error, partner_results) => {
      if (error) {
        throw error;
      }

      // INSERT TO PARTNER_DETAILS 
      var arr = [];
      var str = '';
      partnerDetails.forEach(partnerDetails => {
        var partners = {
          partner_id: partner_results.rows[0].id,
          field_id: partnerDetails.field_id,
          field_value: partnerDetails.field_value,
          updated_on: new Date(Date.now()).toISOString()
        }
        arr.push(partners);
      })
      var values = arr.map(a => {
        return `(${a.partner_id}, ${a.field_id}, '${a.field_value}','${a.updated_on}')`;
      });
      var detailsQuery = `INSERT INTO partnerhandlerschema.partner_details(
        partner_id, field_id, field_value, updated_on) VALUES `;
      str = detailsQuery + values.join(',');

      pool.query(str, (error, results) => {
        if (error) {
          throw error;
        }
        response.json({
          status: "success",
          count: partner_results.rowCount,
          message: `Partner has been added succesfully.`,
          id: partner_results.rows[0].id,
        });
      })
    })
};


const updatePartnerDetails = (request, response) => {
  const { isActive, partnerDetails } = request.body;
  const updatePartnerId = request.params.id;
  if (updatePartnerId) {
    // UPDATE IS_ACTIVE FOR PARTNER 
    var updatePartnerSQL = `UPDATE partnerhandlerschema.partners
    SET is_active = $1, updated_on = $2
    WHERE id = $3 `;
    pool.query(updatePartnerSQL,
      [isActive, new Date(Date.now()).toISOString(), updatePartnerId],
      (error, partner_results) => {
        if (error) {
          throw error;
        }
        // UPDATE BULK PARTNER_DETAILS 
        var arr = [];
        var str = '';
        partnerDetails.forEach(partnerDetails => {
          var partners = {
            field_id: partnerDetails.field_id,
            field_value: partnerDetails.field_value,
            updated_on: new Date(Date.now()).toISOString()
          }
          arr.push(partners);
        })
        var values = arr.map(a => {
          return `(${a.field_id}::integer, '${a.field_value}'::text,'${a.updated_on}'::timestamp)`;
        });
        var detailsQuery = `UPDATE partnerhandlerschema.partner_details p
      SET field_value = v.field_value,
          updated_on = v.updated_on
      FROM (VALUES `+ values.join(',') + `)
        AS v (field_id,field_value, updated_on)
      WHERE p.field_id = v.field_id AND p.partner_id=` + updatePartnerId;

        pool.query(detailsQuery, (error, results) => {
          if (error) {
            throw error;
          }
          response.json({
            status: "success",
            partnerId: updatePartnerId,
            message: `Partner has been updated succesfully.`,
          });
        });
      });
  }
}
const partnerEndpoint = (request, response, next) => {

  if (request.body.hasOwnProperty('partnerEndpointId')) {
    // Update Scenario
    updatePartnerEndpointInfo(request, response, next);
  } else {
    // Insert Scenario
    insertPartnerEndpointInfo(request, response, next);
  }
}

const insertPartnerEndpointInfo = (request, response, next) => {
  try {
    let [
      sourceEndpointId,
      partnerId,
      displayName,
      url,
      createdOn,
      updatedOn
    ] = [
        utility.getObject(request, "body.sourceEndpointId"),
        utility.getObject(request, "body.partnerId"),
        utility.getObject(request, "body.displayName", ""),
        utility.getObject(request, "body.url", ""),
        utility.getObject(request, "body.createdOn", new Date(Date.now()).toISOString()),
        utility.getObject(request, "body.updatedOn", new Date(Date.now()).toISOString()),
      ];

    pool.query(
      "INSERT INTO partnerhandlerschema.partner_endpoints(endpoint_id, partner_id, display_name, url, created_on, updated_on) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
      [
        sourceEndpointId,
        partnerId,
        displayName,
        url,
        createdOn,
        updatedOn
      ],
      (error, results) => {
        if (error) {
          throw error;
        }

        response.json({
          status: "success",
          count: results.rowCount,
          id: results.rows[0].id,
          message: `Partner Endpoint has been added succesfully.`,
        });
      }
    );
  } catch (err) {
    next(err)
  }
}


const updatePartnerEndpointInfo = (request, response, next) => {
  try {
    let [
      partnerEndpointId,
      displayName,
      url,
      updatedOn
    ] = [
        utility.getObject(request, "body.partnerEndpointId"),
        utility.getObject(request, "body.displayName", ""),
        utility.getObject(request, "body.url", ""),
        utility.getObject(request, "body.updatedOn", new Date(Date.now()).toISOString()),
      ];

    pool.query(
      `UPDATE partnerhandlerschema.partner_endpoints 
      SET display_name = $1, url = $2, updated_on = $3
      WHERE id = $4 Returning display_name, url, id as partnerEndpointId`,
      [
        displayName,
        url,
        updatedOn,
        partnerEndpointId
      ], (err, results) => {
        console.log(err, results)
        if (err) {
          throw err;
        } else {
          response.json({ status: 'success', message: 'successfully updated partner endpoint information.', data: results.rows, count: results.rows.length })
        }
      })

  } catch (err) {
    next(err)
  }
}

const savePartnerMappings = (request, response, next) => {
  if (request.body.hasOwnProperty('partnerEndpointId')) {
    // Insert Mappings
    insertPartnerMappings(request, response, next);
  } else {
    // Update Mappings
    updatePartnerMappings(request, response, next);
  }
}

const insertPartnerMappings = (request, response, next) => {
  try {
    const { partnerEndpointId, mappings } = request.body;
    var arr = [];
    var str = '';
    mappings.forEach(mapping => {
      var mappingDetails = {
        partner_endpoint_id: partnerEndpointId,
        key_id: mapping.keyId,
        mapping: mapping.mapping,
        created_on: new Date(Date.now()).toISOString(),
        updated_on: new Date(Date.now()).toISOString()
      }
      arr.push(mappingDetails);
    })
    var values = arr.map(a => {
      return `(${a.partner_endpoint_id}, ${a.key_id}, '${a.mapping}', '${a.created_on}', '${a.updated_on}')`;
    });
    var query = request.params.type === 'request' ?
      'INSERT INTO partnerhandlerschema.request_mappings(partner_endpoint_id, key_id, mapping, created_on, updated_on) VALUES '
      : 'INSERT INTO partnerhandlerschema.response_mappings(partner_endpoint_id, key_id, mapping_value, created_on, updated_on) VALUES '
    str = query + values.join(',');
    pool.query(str,
      (error, results) => {
        if (error) {
          throw error;
        }
        response.json({
          status: "success",
          count: results.rowCount,
          message: `Partner Mappings has been added succesfully.`,
        });
      })
  }
  catch (err) {
    next(err);
  }
}

const updatePartnerMappings = (request, response, next) => {
  try {
    let stringQuery = '';
    const { mappings } = request.body;
    console.log(request.body, request.body.mappings)
    const values = mappings.reduce((acc, curr, index) => acc + '(' + curr.mappingId + `,'` + curr.mapping +
      (index === mappings.length - 1 ? `')` : `'),`), '')


    if (request.params.type === 'request') {
      stringQuery = `UPDATE partnerhandlerschema.request_mappings as t set
        mapping = c.mappingValue FROM (VALUES ${values})
        AS c(mappingId, mappingValue) WHERE c.mappingId  = t.id 
        RETURNING t.id as mappingId, t.mapping;`
    } else if (request.params.type === 'response') {
      stringQuery = `UPDATE partnerhandlerschema.response_mappings as t set
        mapping_value = c.mappingValue FROM (VALUES ${values})
        AS c(mappingId, mappingValue) WHERE c.mappingId  = t.id
        RETURNING t.id as mappingId, t.mapping_value;`
    }
    console.log(stringQuery);
    pool.query(stringQuery, (err, results) => {
      console.log(err, results)
      if (err) {
        throw err;
      } else {
        response.json({ message: 'mappings updated succesfully', status: 'success', data: results.rows, count: results.rowCount.length })
      }
    })
  } catch (err) {
    next(err);
  }
}

const addJoltSchemaForPartnerEndpoint = (request, response, next) => {
  try {
    const values = [request.body.joltSchema, request.body.updatedOn, request.body.partnerEndpointId]

    let stringQuery = request.params.type === 'request' ?
      'UPDATE partnerhandlerschema.partner_endpoints SET request_jolt_schema= $1, updated_on= $2 WHERE id=$3' :
      'UPDATE partnerhandlerschema.partner_endpoints SET response_jolt_schema= $1, updated_on=$2 WHERE id=$3';

    pool.query(stringQuery, values, (err, results) => {
      if (err) {
        throw err;
      }
      response.json({
        status: "success",
        count: results.rowCount,
        message: `Jolt Schema for partner endpoint has been added succesfully.`,
      })
    })
  }
  catch (err) {
    next(err);
  }
}
const signInUser = (request, response, next) => {
  try {
    const { email, password } = request.body;
    response.setHeader("Access-Control-Allow-Origin", "*");
    pool.query(
      `SELECT u.id id, u.user_email, u.is_admin, p.id partner_id 
       FROM partnerhandlerschema.users u , partnerhandlerschema.partners p 
       WHERE u.id = (SELECT id FROM partnerhandlerschema.users WHERE user_email = $1 and user_password = $2)`,
      [email, md5(password)], (err, res) => {
        if (err) {
          throw err;
        }
        else if (!res.rowCount) {
          response.status(404).json({ error: 'error', message: 'Please Check your username or password' });
        } else {
          response.json({
            status: "success",
            count: 1,
            message: `User has been authenticated succesfully.`,
            data: [res.rows[0]]
          })
        }
      }
    )
  }
  catch (err) {
    throw err;
  }
}
const registerUser = (request, response, next) => {
  try {
    const { email, password, createdOn } = request.body;
    response.setHeader("Access-Control-Allow-Origin", "*");
    pool.query(
      "SELECT id FROM partnerhandlerschema.users WHERE user_email = $1",
      [email], (err, res) => {
        if (err) {
          throw err;
        } else if (res.rowCount) {
          response.status(404).json({ error: 'error', message: 'User with same email already exists' });
        } else {

          pool.query(
            "INSERT INTO partnerhandlerschema.users(user_email, user_password, created_on, updated_on, is_admin) VALUES ($1, $2, $3, $4, false) RETURNING id",
            [email, md5(password), createdOn, createdOn], (err, res) => {
              if (err) {
                throw err;
              }
              response.json({
                status: "success",
                count: 1,
                message: `User has registered succesfully.`,
                id: res.rows[0].id
              })
            }
          )
        }
      })
  }
  catch (err) {
    throw err;
  }
}

module.exports = {
  getPartnerDetails,
  createPartner,
  getAvailableEndpoints,
  getPartnerMappings,
  partnerEndpoint,
  savePartnerMappings,
  getKeysbyEndpoint,
  addJoltSchemaForPartnerEndpoint,
  updatePartnerDetails,
  signInUser,
  registerUser,
  getPartnerEndpointInfo,
  getPartnerFields,
  submitPartnerFields
}
