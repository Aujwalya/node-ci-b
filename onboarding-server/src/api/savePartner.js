'use strict'

const serverless = require('serverless-http');
const express = require('express');
const app = express();
const db = require(`../db/query`);
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var cors=require('cors');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0
app.options('*', cors()) // enable pre-flight request for request
app.use(cors())

app.get('/getPartnerDetails/:id', db.getPartnerDetails)
app.get('/getAvailableEndpoints', db.getAvailableEndpoints)
app.get('/getKeysbyEndpoint/:id/:type', db.getKeysbyEndpoint)
app.get('/getPartnerEndpointInfo/:sourceEndpointId/:partnerId', db.getPartnerEndpointInfo)
app.get('/getPartnerMappings/:partnerEndpointId/:type', db.getPartnerMappings)

app.post('/addJoltSchemaForPartnerEndpoint/:type', db.addJoltSchemaForPartnerEndpoint)
app.post('/registerUser', db.registerUser)
app.post('/signInUser', db.signInUser)
app.post('/addPartner', db.createPartner)
app.get('/getPartnerFields', db.getPartnerFields)
app.post('/submitPartnerFields', db.submitPartnerFields)
app.put('/updatePartnerDetails/:id', db.updatePartnerDetails)
app.post('/addPartnerEndpoint', db.partnerEndpoint);
app.post('/addMappings/:type', db.savePartnerMappings)

module.exports.handler = serverless(app)